var express = require('express');
var router = express.Router();
var wrap = require('co-express');
var request = require('request-promise');

/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('index', { title: 'Express' });
});

router.post('/login', wrap(function*(req, res, next) {
  yield request({
    uri: 'http://localhost:3000/API/requestAuth',
    method: 'POST',
    json: req.body
  });
  res.redirect('/auth/' + req.body.phone);
}));

router.get('/auth/:phone', wrap(function*(req, res, next) {
  res.render('login', {
    phone: req.params.phone
  });
}));

router.post('/auth', wrap(function*(req, res, next) {
  console.log(req.body);
  var user = yield request({
    uri: 'http://localhost:3000/API/auth',
    method: 'POST',
    json: req.body
  });
  res.redirect('/test/' + user._id);
}));

router.get('/test/:userId', wrap(function*(req, res, next) {
  res.render('test', {userId: req.params.userId});
}));

router.get('/test-membership/:userId', wrap(function*(req, res, next) {
  var result = yield request("http://localhost:3000/API/checkMembership/" + req.params.userId + "/com.example.test2");
  res.send(result);
}));

router.get('/test-expire/:userId', wrap(function*(req, res, next) {
  var result = yield request("http://localhost:3000/API/checkMonthly/" + req.params.userId + "/com.example.test1");
  res.send(result);
}));

module.exports = router;
